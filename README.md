# webscraping happily with python #

This script gets a bunch of information out of sport websites.

### getting info out of webpages  ###

* Write a small python program accepting a list of urls (separated by a new line) from standard input and produce a json file with the following contents: the input url the title of the article
a list of images with the best url and caption
Example output:


        [
         {"url": "http://www.skysports.com/football/news/11661/10871659/who-won-your-clubs-player-of-the-year-award",
        "headline": "Who won your club's Player of the Year award?",
        "images": [
                   {"url": "http://e1.365dm.com/17/04/16-9/20/skysports-ngolo-kante-chelsea-pfa-award_3937274.jpg?20170423234739", 
                    "caption": "N'Golo Kante with the PFA Players' Player of the Year award"}
                  ]
         }
        ]


Example site: http://www.bbc.co.uk/sport/tennis/37268846

### Testing ###
* requirements 
    * Python 2.7
    * libraries lxml, bs4


        $ pip install lxml
        $ pip install bs4


* python unittest will test that all elements of the json objects have been filled and that urls correspond to actual pages (that will open correctly)

        $ python test_webscraper.py -v