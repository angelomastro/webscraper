#requirements:
# Python 2.7
# pip install lxml
# pip install bs4
from bs4 import BeautifulSoup
import json
import requests
from sys import argv, exit

class WebScraper(object):

    def getPictures(self, outputFile, urlList):
        ret = list()
        for url in urlList:
            page = requests.get(url)
            soup = BeautifulSoup(page.content, "lxml")
            item = dict()
            try:
                item["url"] = url
                item["headline"] = self._getHeadlines(soup)
                article = self._getArticle(soup.find("body"))
                item["images"] = self._getImages(article)
                ret.append(item)
            except Exception as e:
                print "parsing error on url %s: %s" % (url, str(e))
                continue

        with open(outputFile, 'w') as output:
            print "-> produced %s " % outputFile
            json.dump(ret, output)

    def _getHeadlines(self, page):
        title = page.find("head").find("title").text
        return title.split("|")[0].split("-")[0]

    def _getArticle(self, body):
        if body.find("article") is not None:
            return body.find("article")
        return body

    def _getImageUrl(self, img):
        if img.has_attr("src") and img.attrs["src"][:4] == "http":
            return img.attrs["src"]
        return ""

    def _getImageCaption(self, img):
        if img.has_attr("alt"):
            return img.attrs["alt"]
        return ""

    def _getImages(self, article):
        ret = list()
        images = article.find_all("img")
        for img in images:
            imgDict = dict()
            imgDict["url"] = self._getImageUrl(img)
            imgDict["caption"] = self._getImageCaption(img)
            ret.append(imgDict)
        return ret


class SpecificWebScraper(WebScraper):

    def _getHeadlines(self, page):
        article = self._getArticle(page.find("body"))
        if article.find("h1") is not None:
            return article.find("h1").text
        return super(WebScraper, self)._getHeadlines(page)

    def _getImages(self, article):
        ret = list()
        figures = article.find_all("figure")
        for fig in figures:
            imgDict = dict()
            imgDict["url"] = self._getImgUrl(fig)
            imgDict["caption"] = self._getImgCaption(fig)
            ret.append(imgDict)
        if len(ret) == 0: #default to the base class if nothing is found
            return super(WebScraper, self)._getImages(article)
        return ret

    def _getImgUrl(self, figure):
        if figure.find("img") is None:
            return ""
        img = figure.find("img")
        url = super(SpecificWebScraper, self)._getImageUrl(img)
        if url != "":
            return url
        if img.has_attr("src") and img.attrs["src"][:4] == "http":
            return img.attrs["src"]
        if img.has_attr("srcset") and img.attrs["srcset"][:4] == "http":
            return img.attrs["srcset"].split(" ")[0]
        if img.has_attr("data-src") and img.attrs["data-src"][:4] == "http":
            return img.attrs["data-src"]
        #additional logic to add if a website doesn't respect the former
        return ""

    def _getImgCaption(self, figure):
        if figure.find("figcaption") is not None:
            return figure.find("figcaption").text
        if figure.find("img") is None:
            return ""
        # additional logic to add if a website doesn't respect the former
        return super(SpecificWebScraper, self)._getImageCaption(figure.find("img"))


if __name__ == "__main__":
    if len(argv) > 2:
        ws = SpecificWebScraper()
        ws.getPictures(argv[1], argv[2:])
        exit(0)
    print "=== webscraper.py ==="
    print "$ python generic_parser.py <output_file (json)> <url1 [, url2, url3, ....]>"
    exit(1)
