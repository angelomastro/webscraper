from webscraper import SpecificWebScraper

from bs4 import BeautifulSoup
import json
from os import path as ospath, remove as osremove
import requests
import unittest

class TestWebScaper(unittest.TestCase):

    output = "output.json"
    urlList = ["http://www.skysports.com/football/news/11661/10871659/who-won-your-clubs-player-of-the-year-award",
               "http://www.bbc.co.uk/sport/tennis/37268846"]

    @classmethod
    def setUpClass(cls):
        if ospath.isfile(cls.output):
            osremove(cls.output)
        ws = SpecificWebScraper()
        ws.getPictures(cls.output, cls.urlList)

    def test_1_output(self):
        self.assertTrue(ospath.isfile(self.output))

    def test_2_imageFormat(self):
        outputs = list()
        with open(self.output, "r") as input:
            outputs.append(json.loads(input.read()))

        for result in outputs[0]:
            self.assertIn("url", result)
            self.assertIn("headline", result)
            self.assertIn("images", result)

            for img in result["images"]:
                self.assertIn("url", img)
                self.assertNotEqual("", img["url"])

                self.assertIn("caption", img)
                self.assertNotEqual("", img["caption"])

    def test_3_imageUrls(self):
        outputs = list()
        with open(self.output, "r") as input:
            outputs.append(json.loads(input.read()))
        for result in outputs[0]:
            for img in result["images"]:
                try:
                    requests.get(img["url"])
                except:
                    self.fail("failed to open img url: " + img["url"])

if __name__ == '__main__':
    unittest.TestLoader.sortTestMethodsUsing = lambda _, x, y: cmp(x, y)
    unittest.main()
